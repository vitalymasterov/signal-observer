package info.vm.signal.observer;

import static org.jboss.logging.Logger.Level.DEBUG;
import static org.jboss.logging.Logger.Level.INFO;

import org.jboss.logging.BasicLogger;
import org.jboss.logging.Logger;
import org.jboss.logging.annotations.LogMessage;
import org.jboss.logging.annotations.Message;
import org.jboss.logging.annotations.MessageLogger;

@MessageLogger(projectCode = "SIGNAL-OBSERVER-")
public interface SignalObserverLogger extends BasicLogger {
	
	SignalObserverLogger LOGGER = Logger.getMessageLogger(SignalObserverLogger.class, SignalObserverLogger.class.getPackage().getName());

	@LogMessage(level = INFO)
	@Message(id = 1, value = "===== Trying to start signal observer =====")
	void tryStart();
	
	@LogMessage(level = DEBUG)
	@Message(id = 2, value = "===== Signal Observer has been launched on host = %s http.port = %d and ajp.port = %d =====")
	void startSignalObserver(String host, int httpPort, int ajpPort);

	@LogMessage(level = DEBUG)
	@Message(id = 3, value = "===== Signal Observer has been stopped =====")
	void shutdownSignalObserver();

	@LogMessage(level = INFO)
	@Message(id = 4, value = "Hello from Listener ...")
	void helloFromListener();
	
}