package info.vm.signal.observer;

public class ConfigProps {

	private static final String HTTP_PORT = "http.port";
	
	private static final String AJP_PORT = "ajp.port";
	
	private static final String HOST = "host";
	
	private static final String SCRIPTS_PATH = "scripts.path";
	
	private int DERFAULT_HTTP_PORT = 8000;
	
	private int DERFAULT_AJP_PORT = 8001;
	
	private String DEFAULT_HOST = "127.0.0.1";
	
	private String DEFAULT_SCRIPTS_PATH = "/usr/local/signal-observer";
	
	private int httpPort;
	
	private int ajpPort;
	
	private String host;
	
	private String scriptsPath;
	
	public ConfigProps(String[] args) {
		httpPort = DERFAULT_HTTP_PORT;
		ajpPort = DERFAULT_AJP_PORT;
		host = DEFAULT_HOST;
		scriptsPath = DEFAULT_SCRIPTS_PATH;
		for (int i = 0; i < args.length; i++) {
			String currArg = args[i];
			if (args[i].startsWith(HTTP_PORT)) {
				httpPort = Integer.valueOf(currArg.substring(HTTP_PORT.length()+1)).intValue();
			}
			if (args[i].startsWith(AJP_PORT)) {
				ajpPort = Integer.valueOf(currArg.substring(AJP_PORT.length()+1)).intValue();
			}
			if (args[i].startsWith(HOST)) {
				host = currArg.substring(HOST.length()+1);
			}
			if (args[i].startsWith(SCRIPTS_PATH)) {
				scriptsPath = currArg.substring(SCRIPTS_PATH.length()+1);
			}
		}				
	}

	public int getHttpPort() {
		return httpPort;
	}

	public void setHttpPort(int httpPort) {
		this.httpPort = httpPort;
	}

	public int getAjpPort() {
		return ajpPort;
	}

	public void setAjpPort(int ajpPort) {
		this.ajpPort = ajpPort;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getScriptsPath() {
		return scriptsPath;
	}

	public void setScriptsPath(String scriptsPath) {
		this.scriptsPath = scriptsPath;
	}
	
}