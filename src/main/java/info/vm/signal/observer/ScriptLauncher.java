package info.vm.signal.observer;

import static info.vm.signal.observer.SignalObserverLogger.LOGGER;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ScriptLauncher {
	
	private Process process;
	
	private boolean isRunning;

	public void launch(ConfigProps configProps, String command, File workingDirectory) {
		String[] args = new String[]{"sh", command.concat(".sh")};
		File workDir = new File(configProps.getScriptsPath());
		
		ProcessBuilder processBuilder = new ProcessBuilder();
		processBuilder.command(args);
		processBuilder.redirectErrorStream(true);
		processBuilder.redirectInput(ProcessBuilder.Redirect.PIPE);
//		processBuilder.directory(workingDirectory);
		processBuilder.directory(workDir);
		try {
			process = processBuilder.start();
			Thread clThread = new Thread(() -> {
				try (
						InputStream is = process.getInputStream();
						InputStreamReader isr = new InputStreamReader(is);
						BufferedReader br = new BufferedReader(isr);
				) {
					isRunning = true;
					String line;
					while ((line = br.readLine()) != null) {
						LOGGER.debug(line);
					}
				} catch (IOException e) {
					throw new RuntimeException(e.getMessage(), e);
				} finally {
					isRunning = false;
				}
			});
			clThread.setDaemon(true);
			clThread.start();
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

}