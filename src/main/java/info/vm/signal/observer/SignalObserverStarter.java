package info.vm.signal.observer;

import static info.vm.signal.observer.SignalObserverLogger.LOGGER;

import java.util.Deque;

import info.vm.signal.observer.exceptions.NoQualifierException;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

/**
 * Entry Point For Java Launcher
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see ConfigProps
 * @see SignalObserverLogger
 *
 */
public class SignalObserverStarter {
	
	private static final String QUALIFIER = "qualifier";
	
	public static void main(String[] args) {
		ConfigProps configProps = new ConfigProps(args);
		
		LOGGER.tryStart();
        Undertow server = Undertow.builder()
                .addHttpListener(configProps.getHttpPort(), configProps.getHost())
                .setHandler(new HttpHandler() {
                    
                	@Override
                    public void handleRequest(final HttpServerExchange exchange) throws Exception {
                		LOGGER.helloFromListener();
                		
                		Deque<String> qualifierDeque = exchange.getQueryParameters().get(QUALIFIER);
                		if (qualifierDeque == null) {
                			throw new NoQualifierException();
                		}
                		String qualifier = qualifierDeque.poll();
                		LOGGER.info("qualifier = " + qualifier);
                		ScriptLauncher launcher = new ScriptLauncher();
                		launcher.launch(configProps, qualifier, null);
                		
                        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
                        exchange.getResponseSender().send("ok...");
                    }
                	
                }).build();
        server.start();
        LOGGER.startSignalObserver(configProps.getHost(), configProps.getHttpPort(), configProps.getHttpPort());
	}
	
}